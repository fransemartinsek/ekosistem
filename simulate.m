function [A, b, x0, delta] = simulate(n,coefA,coefB,coefX)
% [A, b, x0, delta] = simulate(n), zgenerira tako matriko A in tak
% stolpicni vektor b, za katere velja da je stacionarna tocka pozitivna in
% narise graf za n korakov
% vrne matriko A, vektor b, zacetni priblizek x0, in delta, ki je razlika
% med x0 in stacionarno točko sistema
    
    %handlanje optional parametrov
    if ~exist('coefA','var')
        % parameter does not exist, so default it to something
        coefA = 0.1;
    end
    if ~exist('coefB','var')
        coefB = 1;
    end
    if ~exist('coefX','var')
        coefX = 1.1;
    end
    
    % priprava cilja za shranjevanje primerov
    dirname = 'Saves';
    if not(isfolder(dirname))
        mkdir(dirname);
    end
    lsdir = dir(dirname);
    filename = strcat("save",int2str(size(lsdir,1)-1),".mat");
    
    % generiranje nakljucnega primera
    [A,b,x0] = generate(coefA,coefB);
    
    % stacionarno tocko in malce pokvarimo
    x0 = x0*coefX;

    % razlika med stacionarno tocko, in našo zacetno vrednostjo
    delta = A\-b - x0; 

    % funkcija sistema
    f = @(X) X.*(b + A*X);
    
    save(strcat(dirname,'/',filename), 'A', 'b', 'x0', 'delta');
    
    % Uporabimo metodo Runge-Kutta cetrte stopnje za simulacijo
    % x- zacetna vrednost, v X - vektor iteraciji, h - velikost koraka
    x = x0;
    X = [x];
    h = 0.1;
    for i = 1:n
       k1 = f(x);
       k2 = f(x+h*k1/2);
       k3 = f(x+h*k2/2);
       k4 = f(x+h*k3);
       x = x + h*(k1+k2+k2+k3+k3+k4)/6;
       X = [X x];
    end
    
    % izrišemo rešitve
    clf
    hold on
    for i = 1:6
        plot(X(i,:));
    end
    hold off
end


